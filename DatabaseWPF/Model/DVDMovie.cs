﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseWPF.Model
{
    public class DVDMovie
    {
        public string Title { get; set; }
        public int DVDNumber { get; set; }
        public int CatalogNumber { get; set; }
        public string Status { get; set; }

        public DVDMovie()
        {
            
        }

        public DVDMovie(string title, int dvdnumber, int catalognumber, string status)
        {
            this.Title = title;
            this.DVDNumber = dvdnumber;
            this.CatalogNumber = catalognumber;
            this.Status = status;
        }

        public override string ToString()
        {
            return Title + ", DVD number: " + DVDNumber + ", Catalog number: " + CatalogNumber +
                   ", Status: " + Status;
        }
    }
}
