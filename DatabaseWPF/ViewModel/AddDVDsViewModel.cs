﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class AddDVDsViewModel : ViewModelBase
    {
        private Movie _movie;
        private string _cost;
        private ObservableCollection<Movie> _movies;
        private Movie _checkedMovie;

        public Movie Movie
        {
            get { return _movie; }
            set
            {
                if (Equals(value, _movie)) return;
                _movie = value;
                AddDVDCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Movie));
            }
        }

        public string Cost
        {
            get { return _cost; }
            set
            {
                if (value == _cost) return;
                _cost = ChangeSemi(value);
                AddDVDCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Cost));
            }
        }

        public ObservableCollection<Movie> Movies
        {
            get { return _movies; }
            set
            {
                if (Equals(value, _movies)) return;
                _movies = value;
                OnPropertyChanged(nameof(Movies));
            }
        }

        public Movie CheckedMovie
        {
            get { return _checkedMovie; }
            set
            {
                if (Equals(value, _checkedMovie)) return;
                _checkedMovie = value;
                LoadDVDs();
                OnPropertyChanged(nameof(CheckedMovie));
            }
        }

        public ObservableCollection<DVD> DVDs
        {
            get { return _dvDs; }
            set
            {
                if (Equals(value, _dvDs)) return;
                _dvDs = value;
                OnPropertyChanged(nameof(DVDs));
            }
        }

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public Branch CheckedBranch
        {
            get { return _checkedBranch; }
            set
            {
                if (Equals(value, _checkedBranch)) return;
                _checkedBranch = value;
                LoadMovies();
                OnPropertyChanged(nameof(CheckedBranch));
            }
        }

        public Branch SelectedBranch
        {
            get { return _selectedBranch; }
            set
            {
                if (Equals(value, _selectedBranch)) return;
                _selectedBranch = value;
                LoadMovies2();
                OnPropertyChanged(nameof(SelectedBranch));
            }
        }

        public ObservableCollection<Movie> MoviesToShow
        {
            get { return _moviesToShow; }
            set
            {
                if (Equals(value, _moviesToShow)) return;
                _moviesToShow = value;
                OnPropertyChanged(nameof(MoviesToShow));
            }
        }

        private dvdrent_dbEntities _context;
        private readonly GenericRepository<Movie> _movieGenericRepository;
        private readonly GenericRepository<DVD> _dvdGenericRepository;
        private readonly GenericRepository<Branch> _branchGenericRepository;
        private ObservableCollection<DVD> _dvDs;
        private ObservableCollection<Branch> _branches;
        private Branch _checkedBranch;
        private Branch _selectedBranch;
        private ObservableCollection<Movie> _moviesToShow;

        public AddDVDsViewModel()
        {
            _context = new dvdrent_dbEntities();
            _movieGenericRepository = new GenericRepository<Movie>(_context);
            _dvdGenericRepository = new GenericRepository<DVD>(_context);
            _branchGenericRepository = new GenericRepository<Branch>(_context);
            AddDVDCommand = new CommandBase(AddDVDExecute, o => true);
            LoadBranches();

        }

        private bool AddDVDCanExecute(object obj)
        {
            if (Movie != null && !string.IsNullOrEmpty(Cost))
            {
                return true;
            }
            return false;
        }

        private async void AddDVDExecute(object obj)
        {
            try
            {
                int rand = RandomNumber();
                DVD newDVD = new DVD()
                {
                    DVDNumber = rand,
                    catalogNumber = Movie.catalogNumber,
                    Movie = Movie,
                    cost = Decimal.Parse(Cost),
                    status = "Free"

                };

                _dvdGenericRepository.Insert(newDVD);
                await _dvdGenericRepository.SaveChanges();
                MessageBox.Show("DVD has been inserted", "Information");
                Clear();
                LoadDVDs();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error with inserting dvd");
            }
            
        }

        private void LoadMovies()
        {
            var movies =  _movieGenericRepository.Get(c => c.branchNumber == CheckedBranch.branchNumber);
            if (movies.Any())
            {
                Movies = new ObservableCollection<Movie>(movies);
            }
            else
            {
                MessageBox.Show("There is no movies in this branch!", "Information");
            }
            
        }
        private void LoadMovies2()
        {
            var movies = _movieGenericRepository.Get(c => c.branchNumber == SelectedBranch.branchNumber);
            if (movies.Any())
            {
                MoviesToShow = new ObservableCollection<Movie>(movies);
            }
            else
            {
                MessageBox.Show("There is no movies in this branch!", "Information");
            }

        }


        public CommandBase AddDVDCommand { get; set; }

        private string ChangeSemi(string val)
        {
            if (!val.Any(char.IsLetter))
            {
                string num = val.Replace(",", ".");
                return num;
            }
            MessageBox.Show("You cannot put letters there");
            return "";
        }

        private int RandomNumber()
        {
            Random r = new Random();
            var next = r.Next(1, 2147483646);

            if (IsNumberFree(next))
            {
                return next;
            }
            else
            {
                return RandomNumber();
            }
        }

        private bool IsNumberFree(int number)
        {
            var mem = _dvdGenericRepository.Get(c => c.DVDNumber == number);
            if (mem.Any())
            {

                return false;
            }
            return true;
        }

        private void LoadDVDs()
        {
            if (CheckedMovie != null)
            {
                var dvds = _dvdGenericRepository.Get(c => c.catalogNumber == CheckedMovie.catalogNumber);
                DVDs = new ObservableCollection<DVD>(dvds);
            }
            
        }
        

        private async void LoadBranches()
        {
            var branches = await _branchGenericRepository.Get();

            if (branches.Any())
            {
                Branches = new ObservableCollection<Branch>(branches);
            }
            else
            {
                MessageBox.Show("There is no branches yet!\nPlease add some branches", "Information");
            }
        }

        private void Clear()
        {
            Cost = string.Empty;
        }

    }
}
