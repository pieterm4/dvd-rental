﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DatabaseWPF.View
{
    /// <summary>
    /// Interaction logic for MoviesCategory.xaml
    /// </summary>
    public partial class MoviesCategory : Window
    {
        public MoviesCategory()
        {
            InitializeComponent();
        }

        private void CategoriesViewButton_Click(object sender, RoutedEventArgs e)
        {
            CategoryView catView = new CategoryView();
            catView.Show();
        }

        private void MoviesViewButton_Click(object sender, RoutedEventArgs e)
        {
            MoviesView moviesView = new MoviesView();
            moviesView.Show();
        }

        private void Show_OnClick(object sender, RoutedEventArgs e)
        {
            MoviesOfGivenCategory mov = new MoviesOfGivenCategory();
            mov.Show();
        }
    }
}
