﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class ShowAvailableDVDsViewModel : ViewModelBase
    {
        private ObservableCollection<Movie> _movies;
        private Movie _checkedMovie;
        private ObservableCollection<DVD> _dvDs;

        public ObservableCollection<Movie> Movies
        {
            get { return _movies; }
            set
            {
                if (Equals(value, _movies)) return;
                _movies = value;
                OnPropertyChanged(nameof(Movies));
            }
        }

        public Movie CheckedMovie
        {
            get { return _checkedMovie; }
            set
            {
                if (Equals(value, _checkedMovie)) return;
                _checkedMovie = value;
                LoadAvalaibleDvd();
                OnPropertyChanged(nameof(CheckedMovie));
            }
        }

        public ObservableCollection<DVD> DVDs
        {
            get { return _dvDs; }
            set
            {
                if (Equals(value, _dvDs)) return;
                _dvDs = value;
                OnPropertyChanged(nameof(DVDs));
            }
        }

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public Branch CheckedBranch
        {
            get { return _checkedBranch; }
            set
            {
                if (Equals(value, _checkedBranch)) return;
                _checkedBranch = value;
                LoadMovies();
                OnPropertyChanged(nameof(CheckedBranch));
            }
        }


        private readonly GenericRepository<Movie> _movieGenericRepository;
        private readonly GenericRepository<DVD> _dvdGenericRepository;
        private readonly GenericRepository<Branch> _branchGenericRepository;

        private dvdrent_dbEntities _context = new dvdrent_dbEntities();
        private ObservableCollection<Branch> _branches;
        private Branch _checkedBranch;

        public ShowAvailableDVDsViewModel()
        {
            _movieGenericRepository = new GenericRepository<Movie>(_context);
            _dvdGenericRepository = new GenericRepository<DVD>(_context);
            _branchGenericRepository = new GenericRepository<Branch>(_context);
            LoadBranches();

        }

        private void LoadMovies()
        {
            var movies =  _movieGenericRepository.Get(c => c.branchNumber == CheckedBranch.branchNumber);
            if (movies.Any())
            {
                Movies = new ObservableCollection<Movie>(movies);
            }
        }

        private void LoadAvalaibleDvd()
        {
            if (CheckedMovie != null)
            {
                var dvds =
                _dvdGenericRepository.Get(c => c.catalogNumber == CheckedMovie.catalogNumber && c.status == "Free");
                DVDs?.Clear();
                DVDs = new ObservableCollection<DVD>(dvds);
            }
            

        }

        private async void LoadBranches()
        {
            var branches = await _branchGenericRepository.Get();
            if (branches.Any())
            {
                Branches = new ObservableCollection<Branch>(branches);
            }
        }

    }
}
