﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DatabaseWPF.View;

namespace DatabaseWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RegisterNewCastomerButton_Click(object sender, RoutedEventArgs e)
        {
            RegisterNewCustomerView registerView = new RegisterNewCustomerView();
            registerView.Show();
          
        }

        private void ShowMoviesButton_Click(object sender, RoutedEventArgs e)
        {
            MoviesCategory mv = new MoviesCategory();
            mv.Show();
        }

        private void ShowAvailableCopiesButton_Click(object sender, RoutedEventArgs e)
        {
            DVDView dvdView = new DVDView();
            dvdView.Show();
        }

        private void RentDVDButton_Click(object sender, RoutedEventArgs e)
        {
            RentDVDView rentDvd = new RentDVDView();
            rentDvd.Show();
        }
    }
}
