﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class CategoryViewModel:ViewModelBase
    {
     
        private string _categoryName;
        private ObservableCollection<Category> _categories;

        

        public string CategoryName
        {
            get { return _categoryName; }
            set
            {
                if (value == _categoryName) return;
                _categoryName = value;
                AddCategory.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CategoryName));
            }
        }

        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set
            {
                if (Equals(value, _categories)) return;
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        private readonly dvdrent_dbEntities _context = new dvdrent_dbEntities();
        private readonly GenericRepository<Category> _categoryGenericRepository;

        public CategoryViewModel()
        {
            _categoryGenericRepository = new GenericRepository<Category>(_context);
            AddCategory = new CommandBase(AddCategoryExecute, AddCategoryCanExecute);
            LoadCategories();
        }

        private bool AddCategoryCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(CategoryName))
            {
                return true;
            }
            return false;
            
        }

        private async void AddCategoryExecute(object obj)
        {
            
            Category category = new Category();
            category.categoryNumber = RandomNumber();
            category.name = CategoryName;

            _categoryGenericRepository.Insert(category);
            await _categoryGenericRepository.SaveChanges();
            MessageBox.Show("Category: " + category.name + " has ben inserted", "Insert");
            ResetValues();
            LoadCategories();
          
        }

        public async void LoadCategories()
        {
            var categories = await _categoryGenericRepository.Get();
            Categories = new ObservableCollection<Category>(categories);
        }

        public CommandBase AddCategory { get; set; }

        private bool IsCategoryNumberFree(int number)
        {
            var categories = _categoryGenericRepository.Get(c => c.categoryNumber == number);
            if (!categories.Any())
            {
                return true;
            }
            return false;
        }

        private void ResetValues()
        {
            
            CategoryName = string.Empty;
        }

        private int RandomNumber()
        {
            Random r = new Random();
            var next = r.Next(1, 2147483646);

            if (IsCategoryNumberFree(next))
            {
                return next;
            }
            else
            {
                return RandomNumber();
            }
        }
    }
}
