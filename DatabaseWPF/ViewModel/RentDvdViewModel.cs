﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DatabaseWPF.Model;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class RentDvdViewModel : ViewModelBase
    {
       
        private ObservableCollection<Member> _members;
        private ObservableCollection<DVDMovie> _rentedDvdMovies;


        

        public ObservableCollection<Member> Members
        {
            get { return _members; }
            set
            {
                if (Equals(value, _members)) return;
                _members = value;
                OnPropertyChanged(nameof(Members));
            }
        }

        public ObservableCollection<DVDMovie> RentedDVDMovies
        {
            get { return _rentedDvdMovies; }
            set
            {
                if (Equals(value, _rentedDvdMovies)) return;
                _rentedDvdMovies = value;
                OnPropertyChanged(nameof(RentedDVDMovies));
            }
        }



        public ObservableCollection<Movie> Movies
        {
            get { return _movies; }
            set
            {
                if (Equals(value, _movies)) return;
                _movies = value;
                
                OnPropertyChanged(nameof(Movies));
            }
        }

        public Movie CheckedMovie
        {
            get { return _checkedMovie; }
            set
            {
                if (Equals(value, _checkedMovie)) return;
                _checkedMovie = value;
                RentCommand.OnCanExecuteChanged();
                LoadDvdsFromMovie();
                OnPropertyChanged(nameof(CheckedMovie));
            }
        }

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public ObservableCollection<Staff> Staffs
        {
            get { return _staffs; }
            set
            {
                if (Equals(value, _staffs)) return;
                _staffs = value;
                OnPropertyChanged(nameof(Staffs));
            }
        }

        public Staff CheckedStaff
        {
            get { return _checkedStaff; }
            set
            {
                if (Equals(value, _checkedStaff)) return;
                _checkedStaff = value;
                RentCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedStaff));
            }
        }

        public Branch CheckedBranch
        {
            get { return _checkedBranch; }
            set
            {
                if (Equals(value, _checkedBranch)) return;
                _checkedBranch = value;
                LoadStaffs();
                LoadMembers();
                LoadMovies();
                RentCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedBranch));
            }
        }

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set
            {
                if (value.Equals(_selectedDate)) return;
                _selectedDate = value;
                RentCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(SelectedDate));
            }
        }

        public DVD CheckedDvd
        {
            get { return _checkedDvd; }
            set
            {
                if (Equals(value, _checkedDvd)) return;
                _checkedDvd = value;
                RentCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedDvd));
            }
        }

        public Member CheckedMember
        {
            get { return _checkedMember; }
            set
            {
                if (Equals(value, _checkedMember)) return;
                _checkedMember = value;
                RentCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedMember));
            }
        }

        public ObservableCollection<DVD> Dvds
        {
            get { return _dvds; }
            set
            {
                if (Equals(value, _dvds)) return;
                _dvds = value;
                OnPropertyChanged(nameof(Dvds));
            }
        }


        private dvdrent_dbEntities _context;
        private readonly GenericRepository<Member> _memberGenericRepository;
        private readonly GenericRepository<DVD> _dvdGenericRepository;
        private readonly GenericRepository<Rent> _rentGenericRepository;
        private readonly GenericRepository<Movie> _movieGenericRepository;
        private readonly GenericRepository<Staff> _staffGenericRepository;
        private readonly GenericRepository<BranchMember> _branchmembeGenericRepository;
        private readonly GenericRepository<Branch> _branchGenericRepository;
        private ObservableCollection<Movie> _movies;
        private Movie _checkedMovie;
        private ObservableCollection<Branch> _branches;
        private ObservableCollection<Staff> _staffs;
        private Staff _checkedStaff;
        private Branch _checkedBranch;
        private DateTime _selectedDate;
        private DVD _checkedDvd;
        private Member _checkedMember;
        private ObservableCollection<DVD> _dvds;


        public RentDvdViewModel()
        {
            _context = new dvdrent_dbEntities();
            _memberGenericRepository = new GenericRepository<Member>(_context);
            _dvdGenericRepository = new GenericRepository<DVD>(_context);
            _rentGenericRepository = new GenericRepository<Rent>(_context);
            _movieGenericRepository = new GenericRepository<Movie>(_context);
            _staffGenericRepository = new GenericRepository<Staff>(_context);
            _branchmembeGenericRepository = new GenericRepository<BranchMember>(_context);
            _branchGenericRepository = new GenericRepository<Branch>(_context);

            

            RentCommand = new CommandBase(RentExecute, RentCanExecute);
            SelectedDate = DateTime.Now;
            LoadRented();
            LoadBranches();
        }

        private async void RentExecute(object obj)
        {
            try
            {
                _dvdGenericRepository.Update(CheckedDvd);
                CheckedDvd.status = "Rented";
                //await _dvdGenericRepository.SaveChanges();
                
                Rent rent = new Rent();
                rent.Member = CheckedMember;
                rent.memberNumber = CheckedMember.memberNumber;
                rent.DVD = CheckedDvd;
                rent.DVDNumber = CheckedDvd.DVDNumber;
                rent.Staff = CheckedStaff;
                rent.staffNumber = CheckedStaff.staffNumber;
                rent.dateRentOut = DateTime.Now;
                rent.returnDate = SelectedDate;
                rent.rentalNumber = RandomNumber();
                _rentGenericRepository.Insert(rent);
               // await _rentGenericRepository.SaveChanges();
                await _context.SaveChangesAsync();

                MessageBox.Show("DVD has been rented", "Successful");
                LoadRented();
                Reset();
                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString(), "Error in rented");
            }
            


        }

        private bool RentCanExecute(object obj)
        {
            if (CheckedBranch != null && CheckedMovie != null && CheckedDvd != null && CheckedMember != null &&
                CheckedStaff != null && SelectedDate != null)
            {
                return true;
            }
            return false;
        }

        private void LoadMovies()
        {
            var movies = _movieGenericRepository.Get(c => c.branchNumber == CheckedBranch.branchNumber);
            
            if (movies.Any())
            {
                Movies = new ObservableCollection<Movie>(movies);
            }
            
        }

        private void LoadDvdsFromMovie()
        {
            if (CheckedMovie != null)
            {
                var dvds =
                _dvdGenericRepository.Get(c => c.catalogNumber == CheckedMovie.catalogNumber && c.status == "Free");

                var enumerable = dvds as IList<DVD> ?? dvds.ToList();
                if (enumerable.Any())
                {
                    Dvds = new ObservableCollection<DVD>(enumerable);
                }
                else
                {
                    MessageBox.Show("There is no available copies to rent", "Information");
                }
            }
                

        }

        private void LoadStaffs()
        {
            if (CheckedBranch != null)
            {
                var staffs = _staffGenericRepository.Get(c => c.branchNumber == CheckedBranch.branchNumber);
                Staffs = new ObservableCollection<Staff>(staffs);
            }

            

        }

        private void LoadMembers()
        {
            if (CheckedBranch != null)
            {
                var memberBranch = _branchmembeGenericRepository.Get(c => c.branchNumber == CheckedBranch.branchNumber);
                List<Member> members = new List<Member>();

                foreach (var branchMember in memberBranch)
                {
                    var member = _memberGenericRepository.Get(c => c.memberNumber == branchMember.memberNumber).FirstOrDefault();
                    members.Add(member);
                }

                Members = new ObservableCollection<Member>(members);
            }
        }

        public CommandBase RentCommand { get; set; }

        private async void LoadBranches()
        {
            var branches = await _branchGenericRepository.Get();
            if (branches.Any())
            {
                Branches = new ObservableCollection<Branch>(branches);
            }
        }

        private  void LoadRented()
        {
            var rented = _dvdGenericRepository.Get(c => c.status == "Rented");

            List<DVDMovie> DVDMovies = new List<DVDMovie>();
            

            foreach (var dvd in rented)
            {
                var tmp = _movieGenericRepository.Get(c => c.catalogNumber == dvd.catalogNumber).FirstOrDefault();
                if (tmp != null)
                {
                    DVDMovies.Add(new DVDMovie() { CatalogNumber = tmp.catalogNumber, Title = tmp.title, DVDNumber = dvd.DVDNumber, Status = dvd.status });
                    
                }
                    

                
            }
            RentedDVDMovies = new ObservableCollection<DVDMovie>(DVDMovies);

        }

        private int RandomNumber()
        {
            Random r = new Random();
            var next = r.Next(1, 2147483646);

            if (IsNumberFree(next))
            {
                return next;
            }
            else
            {
                return RandomNumber();
            }
        }

        private bool IsNumberFree(int number)
        {
            var mem = _rentGenericRepository.Get(c => c.DVDNumber == number);
            if (mem.Any())
            {

                return false;
            }
            return true;
        }

        private void Reset()
        {
            //CheckedBranch = null;
            //CheckedDvd = null;
            //CheckedMember = null;
            //CheckedMovie = null;
            //CheckedStaff = null;
            LoadBranches();
            Movies.Clear();
            Dvds.Clear();
            Members.Clear();
            Staffs.Clear();
        }


    }
}
