﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class MoviesViewModel : ViewModelBase
    {
        private string _title;
        private Branch _branch;
        private ObservableCollection<Category> _categories;
        private ObservableCollection<Branch> _branches;

        public string Title
        {
            get { return _title; }
            set
            {
                if (value == _title) return;
                _title = value;
                AddCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Title));
            }
        }

        public string DailyRental
        {
            get { return _dailyRental; }
            set
            {
                if (value == _dailyRental) return;
                _dailyRental = ChangeSemi(value);
                AddCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(DailyRental));
            }
        }


        public Branch Branch
        {
            get { return _branch; }
            set
            {
                if (Equals(value, _branch)) return;
                _branch = value;
                AddCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Branch));
            }
        }

        //Lists
        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set
            {
                if (Equals(value, _categories)) return;
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public Category CheckedCategory
        {
            get { return _checkedCategory; }
            set
            {
                if (Equals(value, _checkedCategory)) return;
                _checkedCategory = value;
                AddCategory.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedCategory));
            }
        }



        public string FirstNameActor
        {
            get { return _firstNameActor; }
            set
            {
                if (value == _firstNameActor) return;
                _firstNameActor = value;
                AddActor.OnCanExecuteChanged();
                OnPropertyChanged(nameof(FirstNameActor));
            }
        }

        public string SecondNameActor
        {
            get { return _secondNameActor; }
            set
            {
                if (value == _secondNameActor) return;
                _secondNameActor = value;
                AddActor.OnCanExecuteChanged();
                OnPropertyChanged(nameof(SecondNameActor));
            }
        }

        public ObservableCollection<Category> CheckedCategories
        {
            get { return _checkedCategories; }
            set
            {
                if (Equals(value, _checkedCategories)) return;
                _checkedCategories = value;
                AddCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedCategories));
            }
        }

        public ObservableCollection<Actor> Actors
        {
            get { return _actors; }
            set
            {
                if (Equals(value, _actors)) return;
                _actors = value;
                OnPropertyChanged(nameof(Actors));
            }
        }

        public Actor CheckedActor
        {
            get { return _checkedActor; }
            set
            {
                if (Equals(value, _checkedActor)) return;
                _checkedActor = value;
                RemoveActor.OnCanExecuteChanged();
                OnPropertyChanged(nameof(CheckedActor));
            }
        }

        public ObservableCollection<Director> Directors
        {
            get { return _directors; }
            set
            {
                if (Equals(value, _directors)) return;
                _directors = value;
                OnPropertyChanged(nameof(Directors));
            }
        }

        public Director CheckedDirector
        {
            get { return _checkedDirector; }
            set
            {
                if (Equals(value, _checkedDirector)) return;
                _checkedDirector = value;
                OnPropertyChanged(nameof(CheckedDirector));
            }
        }


        private readonly GenericRepository<Movie> _movieGenericRepository;
        private readonly GenericRepository<Category> _categoryGenericRepository;
        private readonly GenericRepository<Branch> _branchGenericRepository;
        private readonly GenericRepository<Actor> _actorRepository;
        private readonly GenericRepository<Director> _directorGenericRepository;
        private Category _checkedCategory;
        private string _dailyRental;

        readonly dvdrent_dbEntities context = new dvdrent_dbEntities();
        private string _firstNameActor;
        private string _secondNameActor;
        private ObservableCollection<Category> _checkedCategories;
        private ObservableCollection<Actor> _actors;
        private Actor _checkedActor;
        private ObservableCollection<Director> _directors;
        private Director _checkedDirector;

        public MoviesViewModel()
        {

            _movieGenericRepository = new GenericRepository<Movie>(context);
            _categoryGenericRepository = new GenericRepository<Category>(context);
            _branchGenericRepository = new GenericRepository<Branch>(context);
            _actorRepository = new GenericRepository<Actor>(context);
            _directorGenericRepository = new GenericRepository<Director>(context);
            AddCategory = new CommandBase(AddCategoryExecute, AddCategoryCanExecute);
            AddCommand = new CommandBase(AddCommandExecute, AddCommandCanExecute);
            AddActor = new CommandBase(AddActorExecute, AddActorCanExecute);
            Actors = new ObservableCollection<Actor>();
            CheckedCategories = new ObservableCollection<Category>();
            RemoveActor = new CommandBase(RemoveActorExecute, RemoveActorCanExecute);

            // LoadBranches();
            LoadCategory();
           

        }

        private void RemoveActorExecute(object obj)
        {
            try
            {
                Actors.Remove(CheckedActor);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error in removing actor");
            }
            
        }

        private bool RemoveActorCanExecute(object obj)
        {
            if (CheckedActor != null)
            {
                return true;
            }
            return false;
        }

        private async void AddActorExecute(object obj)
        {
            var actors = _actorRepository.Get(c => c.name.ToLower() == FirstNameActor.ToLower().Trim() && c.surname.ToLower().Trim() == SecondNameActor.ToLower().Trim());
            var enumerable = actors as IList<Actor> ?? actors.ToList();
            if (enumerable.Any())
            {
                Actors.Add(enumerable.FirstOrDefault());
            }
            else
            {
                Actor actor = new Actor();
                actor.name = FirstNameActor.Trim();
                actor.surname = SecondNameActor.Trim();
                var tmp = await _actorRepository.Get();
                actor.actorNumber = tmp.Last().actorNumber + 1;

                _actorRepository.Insert(actor);
                await _actorRepository.SaveChanges();

                Actors.Add(actor);
            }
        }

        private bool AddActorCanExecute(object obj)
        {
            if (!string.IsNullOrEmpty(FirstNameActor) && !string.IsNullOrEmpty(SecondNameActor))
                return true;
            return false;
        }

        private void AddCategoryExecute(object obj)
        {
            try
            {
                CheckedCategories.Add(CheckedCategory);
                Categories.Remove(CheckedCategory);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error in choosing categories");
            }
            
        }

        private bool AddCategoryCanExecute(object obj)
        {
            if (CheckedCategory != null)
            {
                return true;
            }
            return false;
        }

        private async void AddCommandExecute(object obj)
        {
            try
            {
                Movie movie = new Movie();
                movie.title = Title;
                movie.Branch = Branch;
                movie.branchNumber = Branch.branchNumber;
                movie.dailyRent = Decimal.Parse(DailyRental);

                foreach (var checkedCategory in CheckedCategories)
                {
                    movie.Category.Add(checkedCategory);

                }
                foreach (var actor in Actors)
                {
                    movie.Actor.Add(actor);
                }
                movie.catalogNumber = RandomNumber();
                movie.directorNumber = CheckedDirector.directorNumber;
                movie.Director = CheckedDirector;

                _movieGenericRepository.Insert(movie);
                await _movieGenericRepository.SaveChanges();
                MessageBox.Show("Movie has been added", "Successful");
                Reset();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }


        }

        private bool AddCommandCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(Title) || !CheckedCategories.Any() || Branch == null || string.IsNullOrEmpty(DailyRental) || Title == " " || CheckedDirector == null)
            {
                return false;
            }
            return true;
        }

        private async void LoadCategory()
        {
            
                var categories = await _categoryGenericRepository.Get();
                Categories = new ObservableCollection<Category>(categories);

                var branches = await _branchGenericRepository.Get();
                Branches = new ObservableCollection<Branch>(branches);

                var directors = await _directorGenericRepository.Get();
                if (directors.Any())
                {
                    Directors = new ObservableCollection<Director>(directors);
                }


        }



        public CommandBase AddCommand { get; set; }
        public CommandBase AddCategory { get; set; }
        public CommandBase AddActor { get; set; }
        public CommandBase RemoveActor { get; set; }


        

        private string ChangeSemi(string val)
        {
            if (!val.Any(char.IsLetter))
            {
                string num = val.Replace(",", ".");
                return num;
            }
            MessageBox.Show("You cannot put letters there");
            return "";
        }

        private int RandomNumber()
        {
            Random r = new Random();
            var next = r.Next(1, 2147483646);

            if (IsNumberFree(next))
            {
                return next;
            }
            else
            {
                return RandomNumber();
            }
        }

        private bool IsNumberFree(int number)
        {
            var mem = _movieGenericRepository.Get(c => c.catalogNumber == number);
            if (mem.Any())
            {

                return false;
            }
            return true;
        }

        private void Reset()
        {
            Title = string.Empty;
            DailyRental = string.Empty;
            CheckedCategories.Clear();
            Actors.Clear();
            LoadCategory();
        }


    }
}
