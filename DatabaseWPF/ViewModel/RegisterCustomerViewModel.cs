﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class RegisterCustomerViewModel : ViewModelBase
    {
        private string _firstName;
        private string _secondName;
        private string _address;


        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (value == _firstName) return;
                _firstName = value;
                AddMemberCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(FirstName));
            }
        }

        public string SecondName
        {
            get { return _secondName; }
            set
            {
                if (value == _secondName) return;
                _secondName = value;
                AddMemberCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(SecondName));
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                if (value == _address) return;
                _address = value;
                AddMemberCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Address));
            }
        }

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public Branch Branch
        {
            get { return _branch; }
            set
            {
                if (Equals(value, _branch)) return;
                _branch = value;
                AddMemberCommand.OnCanExecuteChanged();
                OnPropertyChanged(nameof(Branch));
            }
        }


        readonly dvdrent_dbEntities context = new dvdrent_dbEntities();

        private readonly GenericRepository<Member> _memberGenericRepository;
        private readonly GenericRepository<Branch> _branchGenericRepository;
        private readonly GenericRepository<BranchMember> _branchMemberGenericRepository;
        private ObservableCollection<Branch> _branches;
        private Branch _branch;

        //Commands
        public CommandBase AddMemberCommand { get; set; }

        public RegisterCustomerViewModel()
        {
            _memberGenericRepository = new GenericRepository<Member>(context);
            _branchGenericRepository = new GenericRepository<Branch>(context);
            _branchMemberGenericRepository = new GenericRepository<BranchMember>(context);
            AddMemberCommand = new CommandBase(AddMemberExecute, o => true);
            LoadBranches();

        }

        private bool AddMemberCanExecute(object obj)
        {
            if (string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(SecondName) || string.IsNullOrEmpty(Address) ||

                Branch == null)
            {
                return false;
            }
            return true;
        }

        private async void AddMemberExecute(object obj)
        {
            try
            {
                Member member = new Member();
                member.name = FirstName;
                member.surname = SecondName;
                member.memberNumber = RandomNumber();
                member.address = Address;


                _memberGenericRepository.Insert(member);

                BranchMember branchMember = new BranchMember();
                branchMember.Branch = Branch;
                branchMember.Member = member;
                branchMember.memberNumber = member.memberNumber;
                branchMember.branchNumber = Branch.branchNumber;
                branchMember.registrationDate = DateTime.Now;


                

               
                _memberGenericRepository.Insert(member);
                _branchMemberGenericRepository.Insert(branchMember);
                await _memberGenericRepository.SaveChanges();
                await _branchGenericRepository.SaveChanges();

                MessageBox.Show("Member has been inserted", "Insert");
                ResetValues();
               

                
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString(), "Error");
            }
            

        }

        public async void LoadBranches()
        {
            var branches = await _branchGenericRepository.Get();
            Branches = new ObservableCollection<Branch>(branches);
        }

        private void ResetValues()
        {
          
            FirstName = String.Empty;
            SecondName = String.Empty;
            Address = String.Empty;

        }

        private bool IsNumberFree(int number)
        {
            var mem = _memberGenericRepository.Get(c => c.memberNumber == number);
            if (mem.Any())
            {
                
                return false;
            }
            return true;
        }

        private int RandomNumber()
        {
            Random r = new Random();
            var next = r.Next(1, 2147483646);

            if (IsNumberFree(next))
            {
                return next;
            }
            else
            {
                return RandomNumber();
            }
        }


    }
}
