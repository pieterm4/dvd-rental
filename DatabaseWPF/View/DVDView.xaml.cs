﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DatabaseWPF.View
{
    /// <summary>
    /// Interaction logic for DVDView.xaml
    /// </summary>
    public partial class DVDView : Window
    {
        public DVDView()
        {
            InitializeComponent();
        }

        private void AddDVDsButton_Click(object sender, RoutedEventArgs e)
        {
            AddDVDView addDvdView = new AddDVDView();
            addDvdView.Show();
        }

        private void ShowAvailableCopies_Click(object sender, RoutedEventArgs e)
        {
            ShowAvailableDVDsView showAvailableDvDs = new ShowAvailableDVDsView();
            showAvailableDvDs.Show();
        }
    }
}
