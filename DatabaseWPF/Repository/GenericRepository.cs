﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace DatabaseWPF.Repository
{
    public class GenericRepository<T>: IGenericRepository<T> where T:class, new()
    {

        private readonly DbSet<T> _entitySet;
        private readonly DbContext _context;

        

        public GenericRepository(DbContext context)
        {
            if(context == null)
                throw new ArgumentNullException(nameof(context));

            _context = context;
            _entitySet = context.Set<T>();
        }

        public async Task<List<T>> Get()
        {
            return await _entitySet.ToListAsync();
        }

        public async Task<T> Get(int id)
        {
            return await _entitySet.FindAsync(id);
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
            return _entitySet.Where(predicate);
        }

        public void Insert(T entity)
        {

           _entitySet.Add(entity);
        }

        public void Update(T entity)
        {
            if(entity != null)
                _entitySet.Attach(entity);
        }

        public void Delete(T entity)
        {
            _entitySet.Remove(entity);
        }

        public async Task SaveChanges()
        {
            await _context.SaveChangesAsync();
        }
      
    }
}
