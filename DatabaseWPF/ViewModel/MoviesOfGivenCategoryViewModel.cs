﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DatabaseWPF.Repository;

namespace DatabaseWPF.ViewModel
{
    public class MoviesOfGivenCategoryViewModel : ViewModelBase
    {
        private ObservableCollection<Branch> _branches;
        private ObservableCollection<Movie> _movies;
        private Branch _selectedBranch;
        private ObservableCollection<Category> _categories;
        private Movie _selectedMovie;

        public ObservableCollection<Branch> Branches
        {
            get { return _branches; }
            set
            {
                if (Equals(value, _branches)) return;
                _branches = value;
                OnPropertyChanged(nameof(Branches));
            }
        }

        public ObservableCollection<Movie> Movies
        {
            get { return _movies; }
            set
            {
                if (Equals(value, _movies)) return;
                _movies = value;
                OnPropertyChanged(nameof(Movies));
            }
        }

        public Branch SelectedBranch
        {
            get { return _selectedBranch; }
            set
            {
                if (Equals(value, _selectedBranch)) return;
                _selectedBranch = value;
                OnPropertyChanged(nameof(SelectedBranch));
            }
        }

        public ObservableCollection<Category> Categories
        {
            get { return _categories; }
            set
            {
                if (Equals(value, _categories)) return;
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public Movie SelectedMovie
        {
            get { return _selectedMovie; }
            set
            {
                if (Equals(value, _selectedMovie)) return;
                _selectedMovie = value;
                OnPropertyChanged(nameof(SelectedMovie));
            }
        }

        public Category SelectedCategory
        {
            get { return _selectedCategory; }
            set
            {
                if (Equals(value, _selectedCategory)) return;
                _selectedCategory = value;
                LoadMovies();
                OnPropertyChanged(nameof(SelectedCategory));
            }
        }


        private dvdrent_dbEntities context;

        private readonly GenericRepository<Branch> _branchGenericRepository;
        private readonly GenericRepository<Movie> _movieGenericRepository;
        private readonly GenericRepository<Category> _categoryGenericRepository;
        private Category _selectedCategory;


        public MoviesOfGivenCategoryViewModel()
        {
            context = new dvdrent_dbEntities();
            _branchGenericRepository = new GenericRepository<Branch>(context);
            _movieGenericRepository = new GenericRepository<Movie>(context);
            _categoryGenericRepository = new GenericRepository<Category>(context);

            LoadBranches();

        }

        private async void LoadBranches()
        {
            var branches = await _branchGenericRepository.Get();
            Branches = new ObservableCollection<Branch>(branches);

            var categ = await _categoryGenericRepository.Get();
            Categories = new ObservableCollection<Category>(categ);

        }


        private void LoadMovies()
        {
            try
            {
                if (SelectedCategory != null)
                {
                    var mov =
                    _movieGenericRepository.Get(
                        c => c.branchNumber == SelectedBranch.branchNumber && c.Category.Any(x => x.categoryNumber == SelectedCategory.categoryNumber));

                    if (mov.Any())
                    {
                        Movies = new ObservableCollection<Movie>(mov);
                    }
                    else
                    {
                        MessageBox.Show("There is no movies with this category in that branch", "Information");
                        Movies.Clear();
                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString(), "Error");
            }
            
            

            

            
        }
    }
}
